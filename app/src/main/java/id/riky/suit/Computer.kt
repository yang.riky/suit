package id.riky.suit

import kotlin.random.Random

class Computer(name: String) : Player(name) {

    fun choose() {
        choiceIndex = Random.nextInt(0,3)
    }
}