package id.riky.suit

abstract class Game(private val name: String) {

    abstract fun getResult(): String
}