package id.riky.suit

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import id.riky.suit.databinding.ActivityGameBinding

class GameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGameBinding
    private val buttonClickedColor = Color.LTGRAY
    private var isVsPlayer: Boolean = false
    private var player = Player("Pemain 1")
    private var player2: Player = Computer("CPU")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        isVsPlayer = intent.getBooleanExtra("isVsPlayer", false)

        player = Player(intent.getStringExtra("name").toString())

        if (isVsPlayer) {
            player2 = Player("Pemain 2")
        }

        binding.apply {
            textViewPlayer.text = player.name
            textViewCom.text = player2.name

            imageButtonClose.setOnClickListener {
                finish()
            }

            imageButtonRefresh.setOnClickListener {
                restart()
            }

            imageButtonRockCom.setOnClickListener {
                imageButtonRockCom.setBackgroundColor(buttonClickedColor)
                playCom("ROCK")
            }

            imageButtonPaperCom.setOnClickListener {
                imageButtonPaperCom.setBackgroundColor(buttonClickedColor)
                playCom("PAPER")
            }

            imageButtonScissorsCom.setOnClickListener {
                imageButtonScissorsCom.setBackgroundColor(buttonClickedColor)
                playCom("SCISSORS")
            }

            imageButtonRockPlayer.setOnClickListener {
                imageButtonRockPlayer.setBackgroundColor(buttonClickedColor)
                play("ROCK")
            }

            imageButtonPaperPlayer.setOnClickListener {
                imageButtonPaperPlayer.setBackgroundColor(buttonClickedColor)
                play("PAPER")
            }

            imageButtonScissorsPlayer.setOnClickListener {
                imageButtonScissorsPlayer.setBackgroundColor(buttonClickedColor)
                play("SCISSORS")
            }
        }

        setButtonPlay(true)
    }

    private fun setTextViewResultStyle(textColor: Int, backgroundColor: Int, textSize: Float) {
        binding.apply {
            textViewResult.setTextColor(textColor)
            textViewResult.setBackgroundColor(backgroundColor)
            textViewResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        }
    }

    private fun setTextViewResult(result: String) {
        binding.apply {
            textViewResult.text = result

            if (result.contains("VS")) {
                setTextViewResultStyle(Color.RED, Color.TRANSPARENT, 60F)
            } else if (result.contains("MENANG")) {
                setTextViewResultStyle(Color.WHITE, Color.GREEN, 24F)
            } else {
                setTextViewResultStyle(Color.WHITE, Color.BLUE, 24F)
            }
        }
    }

    private fun setButtonBackgroundColorComputer(choiceIndex: Int) {
        binding.apply {
            when(choiceIndex) {
                0 -> imageButtonRockCom.setBackgroundColor(buttonClickedColor)
                1 -> imageButtonPaperCom.setBackgroundColor(buttonClickedColor)
                2 -> imageButtonScissorsCom.setBackgroundColor(buttonClickedColor)
            }
        }
    }

    private fun resetButtonBackgroundColor() {
        binding.apply {
            imageButtonRockPlayer.setBackgroundColor(Color.TRANSPARENT)
            imageButtonPaperPlayer.setBackgroundColor(Color.TRANSPARENT)
            imageButtonScissorsPlayer.setBackgroundColor(Color.TRANSPARENT)
            imageButtonRockCom.setBackgroundColor(Color.TRANSPARENT)
            imageButtonPaperCom.setBackgroundColor(Color.TRANSPARENT)
            imageButtonScissorsCom.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    private fun setButtonPlay(x: Boolean) {
        setButtonPlayer(x)

        setButtonCom(x && isVsPlayer)

        if (x) {
            resetButtonBackgroundColor()
        }
    }

    private fun setButtonPlayer(x: Boolean) {
        binding.apply {
            imageButtonRockPlayer.isClickable = x
            imageButtonPaperPlayer.isClickable = x
            imageButtonScissorsPlayer.isClickable = x
        }
    }

    private fun setButtonCom(x: Boolean) {
        binding.apply {
            imageButtonRockCom.isClickable = x
            imageButtonPaperCom.isClickable = x
            imageButtonScissorsCom.isClickable = x
        }
    }

    private fun showResult() {
        if (player.choiceIndex >= 0 && player2.choiceIndex >= 0) {
            val suit = Suit("SUIT", player, player2)
            setTextViewResult(suit.getResult())

            setButtonPlay(false)

            val dialog = AlertDialog.Builder(this)
            dialog.setTitle("Hasil Permainan")
            dialog.setMessage(binding.textViewResult.text.toString())
            dialog.setCancelable(false)

            dialog.setNegativeButton("Main Lagi") {
                    dialogInterface, p1 -> restart()
            }

            dialog.setPositiveButton("Kembali Ke Menu") {
                    dialogInterface, p1 -> finish()
            }

            dialog.show()
        }
    }

    private fun playCom(choice: String) {
        setButtonCom(false)

        player2.choose(choice)
        Toast.makeText(this, "${player2.name} Memilih ${player2.choices[player2.choiceIndex]}", Toast.LENGTH_SHORT).show()

        showResult()
    }

    private fun play(choice: String) {
        setButtonPlayer(false)
        player.choose(choice)
        Toast.makeText(this, "${player.name} Memilih ${player.choices[player.choiceIndex]}", Toast.LENGTH_SHORT).show()

        if (!isVsPlayer) {
            player2.randomChoose()
            setButtonBackgroundColorComputer(player2.choiceIndex)
            Toast.makeText(this, "${player2.name} Memilih ${player2.choices[player2.choiceIndex]}", Toast.LENGTH_SHORT).show()
        }

        showResult()
    }

    private fun restart() {
        player.choiceIndex = -1
        player2.choiceIndex = -1
        setTextViewResult("VS")
        setButtonPlay(true)
    }
}