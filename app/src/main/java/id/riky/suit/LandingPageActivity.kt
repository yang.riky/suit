package id.riky.suit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import id.riky.suit.databinding.ActivityLandingPageBinding

class LandingPageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLandingPageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLandingPageBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val fragments: ArrayList<Fragment> = arrayListOf(
            LandingPage1Fragment(),
            LandingPage2Fragment(),
            LandingPage3Fragment()
        )

        val adapter = ViewPagerAdapter(fragments, this)

        binding.apply {
            viewPager.adapter = adapter

            buttonNext.setOnClickListener{
                if (viewPager.currentItem > 1) {
                    next()
                } else {
                    viewPager.setCurrentItem(viewPager.currentItem + 1, true)
                }
            }
        }
    }

    private fun next() {
        val intent = Intent(this, MenuActivity::class.java)
        intent.putExtra("name", "Riky")
        startActivity(intent)
        finish()
    }
}