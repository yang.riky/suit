package id.riky.suit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import id.riky.suit.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMenuBinding
    private lateinit var name: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        name = intent.getStringExtra("name").toString()

        val snackbar = Snackbar.make(view, "Selamat Datang $name", Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction("Tutup"){
            snackbar.dismiss()
        }
        snackbar.show()

        binding.apply {
            textViewVsPlayer.text = "$name vs Pemain"
            textViewVsCom.text = "$name vs CPU"

            imageViewVsPlayer.setOnClickListener{
                startGame(true)
            }

            imageViewVsCom.setOnClickListener{
                startGame(false)
            }
        }
    }

    private fun startGame(isVsPlayer: Boolean) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("name", name)
        intent.putExtra("isVsPlayer", isVsPlayer)
        startActivity(intent)
    }
}