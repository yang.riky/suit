package id.riky.suit

import kotlin.random.Random

open class Player(val name: String) {

    val choices = arrayOf("ROCK", "PAPER", "SCISSORS")
    var choiceIndex = -1

    fun choose(choice: String) {
        choiceIndex = choices.indexOf(choice.trim().uppercase())
    }

    fun randomChoose() {
        choiceIndex = Random.nextInt(0,3)
    }
}