package id.riky.suit

class Suit(name: String, private var player1: Player, private var player2: Player) : Game(name) {

    override fun getResult(): String {
        var result = " MENANG"
        val diff = player2.choiceIndex - player1.choiceIndex

        result = when (diff) {
            -1,2 -> player1.name + result
            1, -2 -> player2.name + result
            else -> {
                "DRAW"
            }
        } + "!"

        return result
    }
}